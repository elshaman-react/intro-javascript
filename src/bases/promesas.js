import getHeroByOwner,{getHeroSimplified} from "./bases/import-export";

const promesa = new Promise((resolve, reject) => {
    setTimeout(()=> {
        //console.log(' 2 segs luego.....')
        const heroesMarvel = getHeroByOwner('Marvel')
        console.table(heroesMarvel)
        const heroe1 = getHeroSimplified(1)
        //console.table(heroe1)
        resolve(heroe1);
    }, 2000)
});

promesa.then((heroe)=>{
    console.log('Then de la promesa')
    console.log(heroe)
})