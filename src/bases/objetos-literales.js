
const persona = {
    nombre: 'Tony',
    apellido: 'Stark',
    edad: 45,
    direccion: {
        ciudad: 'New York',
        zip: 5556677,
        lat: 14.3232,
        lng: 34.9233
    }
}


//operador spread: me extrae las propiedades del objeto para ponerlos en un nuevo objeto
const persona2 = {...persona}
persona2.nombre = "peter"

console.table(  persona )
console.table(  persona2 )