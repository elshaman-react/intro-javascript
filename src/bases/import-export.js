//importacion cuando se hace directamente sobre el arreglo heroes
//import {heroes} from './data/heroes';

//IMPORTACION POR DEFECTO: LE PUEDE PONER CUALQUIER NOMBRE AL EXPORTADO
import heroes from '../data/heroes';



/*const getHeroById = (id) => (
    heroes.find((heroe) => {
        if(heroe.id === id){
            return true;
        }else{
            return false;
        }
    })
)*/

const getHeroSimplified = (id) =>{
    return heroes.find((heroe)=>heroe.id === id)
}

//const getHeroSimplified = (id) => heroes.find((heroe)=> heroe.id === id)
const getHeroByOwner = ( owner ) => heroes.filter((heroe)=> heroe.owner === owner)


export { getHeroByOwner as default , getHeroSimplified };