const persona ={
    nombre: "Steve",
    edad: 125,
    clave: "Captain America"
}

//Desestructuro sintaxis objetos
const {nombre1, edad, clave}= persona;

console.log(nombre);
console.log(clave)
console.log(edad);

// Desestructuracion en los parametros de una funcion
//donde rango no esta fdefinido en persona, pero por defecto se puede agregar a la desestructuracion
const Contenido = ({clave, edad , nombre , rango = "Capitan"}) =>
    (
        {
            nombreClave: clave,
            anios : edad,
            geo: {
                lat:14.67,
                lng: 34.89
            }

        }
    )

//desestructuracion interna al objeto interno geolocalizacion
const {nombreClave , anios, geo:{lat , lng} } = Contenido(persona);
console.log(nombreClave, anios )
console.table(lat , lng)
